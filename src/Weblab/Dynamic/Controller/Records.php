<?php

namespace Weblab\Dynamic\Controller;

use Base\Entity\Tables;
use Weblab\Dynamic\Entity\Tables as TablesEntity;
use Weblab\Dynamic\Service\Admin;

class Records
{
    protected $_one = null;
    protected $_multiple = null;
    protected $_url = null;
    protected $_i18n = false;

    protected $_maestro = null;

    function __construct(Tables $tables)
    {
        parent::__construct();

        $this->admin = new Admin();

        $this->admin->setRouterId(router()->get("id"))
            ->setRouterRecord(router()->get("record"))
            ->setTableEntity($tables)
            ->setEntity($this->getEntity())
            ->setForm(form())
            ->prepare();
    }

    function getEntity($e = null)
    {
        if ($e) {
            return entity($e);
        }

        return new TablesEntity;
    }

    function allWhereAction($where = [])
    {
        return $this->admin->allWhereAction();
    }

    function allPrepare()
    {
        return $this->admin->allPrepare();
    }

    function allAction()
    {
        return $this->admin->allWhereAction();
    }

    function addPrepare()
    {
        return $this->admin->addPrepare();
    }

    function addAction($_data = [])
    {
        return $this->admin->addAction();
    }

    function editPrepare()
    {
        return $this->admin->editPrepare();
    }

    function editAction($_data = [])
    {
        return $this->admin->editAction();
    }

    function editI18nAction($i18nRelation)
    {
        return $this->admin->editI18nAction($i18nRelation);
    }

    function getRelatedAllAction($arrControllers)
    {
        return $this->admin->getRelatedAllAction($arrControllers);
    }

    function getRelatedEditAction()
    {
        return $this->admin->getRelatedEditAction();
    }

    function deletePrepare()
    {
        return $this->admin->deletePrepare();
    }

    function deleteAction()
    {
        return $this->admin->deleteAction();
    }

    function viewPrepare()
    {
        return $this->admin->viewPrepare();
    }

    function viewAction($_view)
    {
        return $this->admin->viewAction();
    }

    function mtmPrepare()
    {
        return $this->admin->mtmPrepare();
    }

    function mtmAction()
    {
        return $this->admin->mtmAction();
    }
}