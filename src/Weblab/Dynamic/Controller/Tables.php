<?php

namespace Weblab\Dynamic\Controller;

use Pckg\Concept\Reflect;
use Weblab\Dynamic\Service\Command\Table;
use Weblab\Maestro\Controller\Maestro;

class Tables
{

    protected $_one = null;
    protected $_multiple = null;
    protected $_url = null;
    protected $_i18n = false;

    function setRecordByRouterID() {
        $this->setRecordByID(router()->get("id"));
        if (!$this->record) throw new \Exception("Record not found");

    }

    function setRecordByID($id) {
        $this->record = $this->getEntity()->findID($id);

        return $this->record;
    }

    function setFormByRecord() {
        $this->form = form()->{$this->record->getId() ? 'editAction' : 'addAction'}($this->record);
    }

    function getEntity($e = null) {
        $entity = entity($e);
        return $entity;
    }

    function fetchRequest() {
        $_maestro = Reflect::create(Maestro::class);

        if (request()->isPost()) {
            if ($this->form->isValidPost($invalid)) {
                $this->record->setArray($this->form->getData($this->record));

                $_maestro->{$this->record->getId() ? 'update' : 'insert'}($this->record, $this->_url, true);
            } else if ($this->form->isSubmitted()) {
                $this->form->showRequestData()->showErrors();
            }
        }
    }

    function allWhereAction($where = []){
        $entity = $this->getEntity();

        return (new Table())
            ->setEntity($entity->whereArr($where))
            ->setAllFromEntity()
            ->setTitle($this->_multiple)
            ->execute();
    }

    function allAction(){
        $entity = $this->getEntity();

        return (new Table())
            ->setEntity($entity)
            ->setAllFromEntity()
            ->setTitle($this->_multiple)
            ->execute();
    }

    function addPrepare() {
        $this->record = $this->getRecord();
        $this->setFormByRecord();
    }

    function addAction($_data = []) {
        $this->fetchRequest();
        $_maestro = Reflect::create(Maestro::class);

        if (!isset($_data['title'])) $_data['title'] = __($this->_one);
        if (!isset($_data['content'])) $_data['content'] = $this->form->toHTML();

        return $_maestro->add($_data);
    }

    function editPrepare() {
        $this->setRecordByRouterID();
        $this->setFormByRecord();
    }

    function editAction($_data = []) {
        $this->fetchRequest();
        $_maestro = Reflect::create(Maestro::class);

        if (!isset($_data['title'])) $_data['title'] = __($this->_one);
        if (!isset($_data['content'])) $_data['content'] = $this->form->toHTML();

        return $_maestro->edit($_data);
    }

    function deletePrepare() {
        $this->setRecordByRouterID();
    }

    function deleteAction($_maestro) {
        return $_maestro->delete($this->record, $this->_url, false);
    }

    function viewPrepare() {
        $this->setRecordByRouterID();
        $this->setFormByRecord();
    }

    function __construct()
    {
        $this->_one = __('table');
        $this->_multiple = __('table');
        $this->_url = 'dynamic/tables';
    }

    /* list table tables with edit, delete and list buttons */
    static function view($rTable)
    {
        return (new Table())
            ->setTable($rTable)
            ->setAllFromTable()
            ->execute();
    }

    function viewAction()
    {
        return (new Table())
            ->setTable($this->record)
            ->setAllFromTable()
            ->execute();
    }
}

?>