<?php

namespace Weblab\Dynamic\Entity;

class Fields extends \Base\Entity\TableFields
{

    public function __construct()
    {
        parent::__construct();

        $this->with('table_field_type');
    }

    function findList()
    {
        return $this->join(new Tables)
            ->select('CONCAT( tables.table,  \'.\', table_fields.field ) AS title')
            ->where(self::TABLE_FIELD_TYPE_ID, [1, 11])
            ->findList();
        // @T00D00
    }

    public function getFieldsByTable($table)
    {
        return $this->where(Fields::TABLE_ID, $table->getId())->findAll();
    }
}