<?php

namespace Weblab\Dynamic\Form;

use Pckg\Htmlbuilder\Bootstrap\Button;
use Pckg\Htmlbuilder\Bootstrap\Group;
use Pckg\Htmlbuilder\Bootstrap\Input;
use Pckg\Htmlbuilder\Bootstrap\Select;
use Pckg\Htmlbuilder\Bootstrap\Textarea;
use Pckg\Htmlbuilder\Validator;

class FieldTypes extends \Htmlbuilder\Bootstrap\Form
{
    function addAction($table)
    {
        return $this->editAction($table);
    }

    function editAction($t)
    {
        $this->addToFieldset([
            new Input\ID($t),
            $title = new Input\Text("title", $t),
            $slug = new Input\Text("slug", $t),
            new Group([new Button\Submit(), new Button\Cancel()]),
        ]);

        $title->addValidator(Validator::REQUIRED);
        $slug->addValidator(Validator::REQUIRED);

        return $this;
    }
}