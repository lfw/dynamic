<?php

namespace Weblab\Dynamic\Form;

use Pckg\Htmlbuilder\Bootstrap\Button;
use Pckg\Htmlbuilder\Bootstrap\Group;
use Pckg\Htmlbuilder\Bootstrap\Input;
use Pckg\Htmlbuilder\Bootstrap\Select;
use Pckg\Htmlbuilder\Bootstrap\Textarea;
use Pckg\Htmlbuilder\Validator;
use Weblab\Dynamic\Entity\FieldTypes;
use Weblab\Dynamic\Entity\Tables;

class Fields extends \Htmlbuilder\Bootstrap\Form
{
    function addAction($field)
    {
        return $this->editAction($field);
    }

    function editAction($field)
    {
        $this->addToFieldset([
            new Input\ID($field),
            $title = new Input\Text("title", $field),
            $field2 = new Input\Text("field", $field),
            $fieldType = new Select("table_field_type_id", $field),
            $table = new Select("table_id", $field),
            $shown = new Input\Checkbox("shown", $field),
            new Group([new Button\Submit(), new Button\Cancel()]),
        ]);

        $title->addValidator(Validator::REQUIRED);
        $field2->addValidator(Validator::REQUIRED);

        $fieldType->setOptions(FieldTypes::inst()->findList())->addValidator(Validator::REQUIRED);
        $table->setOptions(Tables::inst()->findList())->addValidator(Validator::REQUIRED);

        return $this;
    }
}