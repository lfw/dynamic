<?php

namespace Weblab\Dynamic\Form;

use Pckg\Htmlbuilder\Element;
use Pckg\Htmlbuilder\Element\Form;
use Pckg\Database\Record;
use Pckg\FrameworkLang;
use Weblab\Auth\Service\Auth;
use Weblab\Dynamic\Entity\Relations;

class Records extends Form\Bootstrap
{

    protected $fieldset = null;

    function addAction($table, $record)
    {
        return $this->editAction($table, $record);
    }

    function editAction($table, $record)
    {
        if (!$record) {
            throw new \Exception("Record not found");
        }

        $this->setRecord($record);

        $arrFields = entity('Fields')->getFieldsByTable($table);

        $this->fieldset = $this->addFieldset();

        $buttonset = $this->addFieldset();
        $buttonset->addSubmit();

        foreach ($arrFields AS $field) {
            if ($field->getField() == 'language_id') {
                if ($record->getLanguageId()) {
                    //$slug = 'hidden';
                } else {
                    $record->setLanguageId(Lang::DEF);
                }
            }

            $this->addToFieldset($field, $record);
        }

        return $this;
    }

    function mtmAction($table, Record $record)
    {
        $arrBelongsTo = $record->getEntity()->_getBelongsTo();

        if (count($arrBelongsTo) < 2) {
            throw new Exception("Less than 2 belongs to relations found", 1);
        }

        $firstBelongsTo = array_shift($arrBelongsTo);
        $secondBelongsTo = array_shift($arrBelongsTo);

        $firstEntity = entity($firstBelongsTo['entity']);
        $secondEntity = entity($secondBelongsTo['entity']);

        // get all records for both relations

        $arrFirstData = $firstEntity->findListID();
        $arrSecondData = $secondEntity->findListID();

        $table = new Element();
        $table->setTag('table');
        $table->addClass('table table-condensed table-hover table-responsive table-bordered');

        // get all permissions
        $arrPermissions = $record->getEntity()->findAll();
        $matrixPermissions = [];
        foreach ($arrPermissions AS $rPermission) {
            foreach (["edit", "add", "view", "delete"] AS $perm) {
                $matrixPermissions[$rPermission->get($firstBelongsTo['current'])][$rPermission->get($secondBelongsTo['current'])][$perm] = $rPermission->get($perm);
            }
        }

        // create headings
        foreach ($arrFirstData AS $rowId => $row) {
            $tr = new Element();
            $tr->setTag('tr');

            $td = new Element();
            $td->setTag('th');
            $td->addChild(' ');

            $tr->addChild($td);

            foreach ($arrSecondData AS $colId => $col) {
                $td = new Element();
                $td->setTag('td');
                $td->addChild($col . '(' . $colId . ')');
                $tr->addChild($td);
            }

            $table->addChild($tr);

            break;
        }

        // create body
        foreach ($arrFirstData AS $rowId => $row) {
            $tr = new Element();
            $tr->setTag('tr');

            $td = new Element();
            $td->setTag('th');
            $td->addChild($row . '(' . $rowId . ')');

            $tr->addChild($td);

            foreach ($arrSecondData AS $colId => $col) {
                $td = new Element();
                $td->setTag('th');
                $tr->addChild($td);

                foreach (["edit", "add", "view", "delete"] AS $perm) {
                    $checkbox = new Element\Input\Checkbox();
                    $checkbox->addDecoratorBootstrap();
                    $checkbox->setLabel($perm);
                    $checkbox->setName($perm . '[' . $rowId . ']' . '[' . $colId . ']');
                    $checkbox->setChecked(isset($matrixPermissions[$rowId][$colId][$perm]));
                    $td->addChild($checkbox);
                }
            }

            $table->addChild($tr);
        }

        // add buttons

        $this->fieldset = $this->addFieldset();
        $this->fieldset->addChild($table);

        $buttonset = $this->addFieldset();
        $buttonset->addSubmit();

        return $this;
    }

    function getData($table = null, $record = null)
    {
        $arrData = $_POST[lcfirst(str_replace(['\\', 'Record'], '', get_class($record)))];
        $arrFields = entity('Fields')->getFieldsByTable($table);

        foreach ($arrFields AS $field) {
            $slug = $field->getTableFieldType()->getSlug();

            if ($slug == 'password') {
                if (array_key_exists($field->getField(), $arrData) && empty($arrData[$field->getField()])) {
                    unset($arrData[$field->getField()]);
                } else if (array_key_exists($field->getField(), $arrData)) {
                    $arrData[$field->getField()] = Auth::makePassword($arrData[$field->getField()]);
                }
            } else if ($slug == 'file' || $slug == 'picture') {

                $class = lcfirst(str_replace(['\\', 'Record'], '', get_class($record)));

                if (isset($_FILES[$class]["name"][$field->getField()])) {
                    $file = [
                        "name" => $_FILES[$class]["name"][$field->getField()],
                        "type" => $_FILES[$class]["type"][$field->getField()],
                        "tmp_name" => $_FILES[$class]["tmp_name"][$field->getField()],
                        "error" => $_FILES[$class]["error"][$field->getField()],
                        "size" => $_FILES[$class]["size"][$field->getField()],
                    ];

                    if ($slug == 'picture') {
                        $extension = strtolower(end(explode(".", $file['name'])));
                        if (!in_array($extension, ["png", "jpg", "gif", "jpeg", "bmp"])) {
                            break;
                        }
                    }

                    $dir = $field->getTable()->getTable() . '/';

                    if (!is_dir(path('uploads') . $dir)) {
                        mkdir(path('uploads') . $dir, 0775, true);
                    }

                    $i = '';
                    while (file_exists(path('uploads') . $dir . ($i ? $i . "-" : '') . $file['name'])) {
                        $i++;
                    }

                    $relativeUrl = path('uploads') . $dir . $i . $file['name'];

                    $newPath = str_replace(path('www'), '/', $relativeUrl);

                    move_uploaded_file($file["tmp_name"], $relativeUrl);

                    $arrData[$field->getField()] = $newPath;
                }
            }
        }

        return $arrData;
    }

    function addToFieldset($field, $record, Element $element = null)
    {
        $slug = $field->getTableFieldType()->getSlug();

        $element = $this->fieldset->addMapped($slug, $field->getField());

        if ($slug == 'foreign') {
            $this->fillForeign($field, $element);
        }

        if ($element->getAttribute('type') != 'hidden') {
            $this->fillLabel($field, $element);
        }

        if ($field->isRequired()) {
            $element->required();
        }

        return $element;
    }

    protected function fillForeign($field, $element)
    {
        $relation = Relations::inst()
            ->where('table1_id', $field->getTableId())
            ->where('table_field1_id', $field->getId())
            ->findOne();

        $relatedTable = $relation->getTable2();
        $relatedEntity = $relatedTable->getEnt();
        $relatedEntity = new $relatedEntity;

        if (!$relation->isRequired()) {
            $element->addOptions([null => ' - - - ']);

        }

        if ($relatedTable->getTableDisplayType()->getSlug() == 'tree') {
            $treeField = null;

            if ($relatedTable->getTable() == 'menus') {
                $treeField = 'getMenuId';

            } else if ($relatedTable->getTable() == 'routes') {
                $treeField = 'getParentRouteId';

            } else if ($relatedTable->getTable() == 'contents') {
                $treeField = 'getParentContentId';

            } else if ($relatedTable->getTable() == 'settings') {
                $treeField = 'getSettingId';

            }

            $arrOptions = $relatedEntity->findAll()->getTree($treeField);

            $element->addTreeOptions($arrOptions);

        } else if ($relation->getTable2()->getTable() == 'table_fields') {
            $element->addOptions($relatedEntity->findCustomList(function ($row) {
                return $row->getTable()->getTitle() . " - " . $row->getTitle();
            }));

        } else {
            $element->addOptions($relatedEntity->findListId());

        }
    }

    protected function fillLabel($field, $element)
    {
        $element->setLabel($field->getTitle());
    }

    /*public function onSubmit() {
        $this->useRequestDatasource();
    }

    public function onValid() {

    }*/

}