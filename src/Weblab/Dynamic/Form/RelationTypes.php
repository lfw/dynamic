<?php

namespace Weblab\Dynamic\Form;

use Pckg\Htmlbuilder\Bootstrap\Button;
use Pckg\Htmlbuilder\Bootstrap\Group;
use Pckg\Htmlbuilder\Bootstrap\Input;
use Pckg\Htmlbuilder\Bootstrap\Select;
use Pckg\Htmlbuilder\Bootstrap\Textarea;
use Pckg\Htmlbuilder\Validator;

class RelationTypes extends \Htmlbuilder\Bootstrap\Form
{
    function addAction($relationType)
    {
        return $this->editAction($relationType);
    }

    function editAction($relationType)
    {
        $this->addToFieldset([
            new Input\ID($relationType),
            $title = new Input\Text("title", $relationType),
            $slug = new Input\Text("slug", $relationType),
            new Group([new Button\Submit(), new Button\Cancel()]),
        ]);

        $title->addValidator(Validator::REQUIRED);
        $slug->addValidator(Validator::REQUIRED);

        return $this;
    }
}