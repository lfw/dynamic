<?php

namespace Weblab\Dynamic\Form;

use Pckg\Htmlbuilder\Bootstrap\Button;
use Pckg\Htmlbuilder\Bootstrap\Button\Cancel;
use Pckg\Htmlbuilder\Bootstrap\Button\Submit;
use Pckg\Htmlbuilder\Bootstrap\Group;
use Pckg\Htmlbuilder\Bootstrap\Input;
use Pckg\Htmlbuilder\Bootstrap\Input\ID;
use Pckg\Htmlbuilder\Bootstrap\Input\Text;
use Pckg\Htmlbuilder\Bootstrap\Select;
use Pckg\Htmlbuilder\Bootstrap\Textarea;
use Pckg\Htmlbuilder\Validator;
use Weblab\Dynamic\Entity\Fields;
use Weblab\Dynamic\Entity\RelationTypes;
use Weblab\Dynamic\Entity\Tables;

class Relations extends \Htmlbuilder\Bootstrap\Form
{
    function addAction($relation)
    {
        return $this->editAction($relation);
    }

    function editAction($relation)
    {
        $this->addToFieldset([
            new ID($relation),
            $title = new Text("title", $relation),
            $table1 = new Select("table1_id", $relation),
            $tableField1 = new Select("table_field1_id", $relation),
            $relationType = new Select("table_relation_type_id", $relation),
            $table2 = new Select("table2_id", $relation),
            $tableField2 = new Select("table_field2_id", $relation),

            new Group([new Submit(), new Cancel()]),
        ]);

        $title->addValidator(Validator::REQUIRED);

        $table1->setOptions(Tables::inst()->findList())->addValidator(Validator::REQUIRED);
        $tableField1->setOptions(Fields::inst()->makeSyncList())->addValidator(Validator::REQUIRED);
        $relationType->setOptions(RelationTypes::inst()->findList())->addValidator(Validator::REQUIRED);
        $table2->setOptions(Tables::inst()->findList())->addValidator(Validator::REQUIRED);
        $tableField2->setOptions(Fields::inst()->makeSyncList())->addValidator(Validator::REQUIRED);

        return $this;
    }
}