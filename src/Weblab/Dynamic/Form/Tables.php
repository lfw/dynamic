<?php

namespace Weblab\Dynamic\Form;

class Tables extends \Htmlbuilder\Element\Form\Bootstrap
{
    function addAction($table)
    {
        return $this->editAction($table);
    }

    function editAction($t)
    {
        $this->setRecord($t);

        $fieldset = $this->addFieldset();

        $fieldset->addText('title');
        $fieldset->addText('table');
        $fieldset->addText('ent');

        return $this;
    }
}