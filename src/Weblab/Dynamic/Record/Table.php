<?php

namespace Weblab\Dynamic\Record;


use Base\Entity\TablePermissions;
use Weblab\Auth\Service\Auth;

class Table extends \Base\Record\Table
{

    function checkPermissions($type = 'view')
    {
        return true;

        if (Auth::getUser()->getId() == 1) {
            return true;
        }
        $permissions = TablePermissions::inst()
            ->where(TablePermissions::TABLE_ID, $this->getId())
            ->where(TablePermissions::USER_STATUS_ID, Auth::getUser()->getStatusId())
            ->findOne();
        $privileged = false;

        return true;

        if (!$permissions) {
            throw new Exception\NotFound("Permissions for table not found (" . $this->getId() . " " . 1 . ")");

        } else if ($type == 'view') {
            $privileged = $permissions->isView();

        } else if ($type == 'edit') {
            $privileged = $permissions->isEdit();

        } else if ($type == 'delete') {
            $privileged = $permissions->isDelete();

        } else if ($type == 'add') {
            $privileged = $permissions->isAdd();

        }

        if ($privileged) {
            return true;
        }

        throw new Exception\NotFound("Permission denied for table (" . $this->getId() . " " . 1 . ")");
    }

}