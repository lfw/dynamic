<?php

namespace Weblab\Dynamic\Service;

use Pckg\Concept\Reflect;
use Weblab\Dynamic\Entity\Relations as ERelations;
use Weblab\Dynamic\Entity\Tables;
use Weblab\Maestro\Controller\Maestro;

class Admin
{
    protected $_one = null;
    protected $_multiple = null;
    protected $_url = null;
    protected $_i18n = false;

    public $_maestro = null;

    function __construct()
    {
        $this->_maestro = Reflect::create(Maestro::class);
    }

    /* config */
    function setRouterId($routerId)
    {
        $this->routerId = $routerId;

        return $this;
    }

    function setRouterRecord($routerRecord)
    {
        $this->routerRecord = $routerRecord;

        return $this;
    }

    function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }

    function setForm($form)
    {
        $this->form = $form;

        return $this;
    }

    function setTableEntity($tableEntity)
    {
        $this->tableEntity = $tableEntity;

        return $this;
    }

    /* service */

    function prepare()
    {
        if ($this->routerId) {
            $this->table = $this->tableEntity->where('id', $this->routerId)->findOne();
            if (!$this->table) throw new \Exception("Table #" . $this->routerId . " not found");

            $this->_one = $this->table->getTable();
            $this->_multiple = $this->table->getTable();
            $this->_url = 'dynamic/tables/' . $this->table->getId();
        }

        $this->record = $this->routerRecord
            ? $this->getEntity($this->table->getEnt())->where('id', $this->routerRecord)->findOne()
            : $this->getEntity($this->table->getEnt())->getEmptyRecord();

        if (!$this->record) throw new \Exception("Record #" . $this->routerRecord . " not found");
    }

    function setFormByRecord()
    {
        $this->form->{$this->record->getId() ? 'editAction' : 'addAction'}($this->table, $this->record)->setAction("/dynamic/tables/" . $this->table->getId() . '/' . ($this->record->getId() ? "edit/" . $this->record->getId() : 'add'));
    }

    function prepareFormI18n()
    {
        return $this;
    }

    function getEntity($e = null)
    {
        if ($e) {
            return entity($e);
        }

        return new Tables;
    }

    function fetchRequest()
    {
        if (request()->isPost()) {
            echo "Request is post\n";
            if ($this->form->isSubmitted()) {
                echo "Form is submitted, setting request datasource\n";
                // fill form with posted data
                //$this->form->useRequestDatasource();
                echo "Request datasource used\n";

                if ($this->form->isValidPost()) {
                    die("Post is valid\n");
                    $this->record->setArray($this->form->getData($this->table, $this->record));

                    $this->_maestro->{$this->record->getId() ? 'update' : 'insert'}($this->record, $this->_url, true);
                } else {
                    die("Post is invalid\n");
                    $this->form->fillPostData()->showErrors();
                }
            }
        } else {
            $this->form->useRecordDatasource();
        }
    }

    function allWhereAction($where = [])
    {
        return self::allWhere($this->entity, $where, [
            "title" => $this->_multiple,
            "heading" => $arrHeading
        ]);
    }

    static function allWhere($entity, $where, $conf)
    {
        $arrConstants = ['slug', 'title', 'email', 'name', 'key'];
        $arrHeading = ['id' => '#'];

        foreach ($arrConstants AS $constant) {
            if (defined(get_class($entity) . '::' . strtoupper($constant))) {
                $arrHeading[$constant] = $constant;
            }
        }

        $conf['heading'] = $arrHeading;
        $_maestro = Reflect::create(Maestro::class);

        return $_maestro->allForeign($entity->whereArr($where)->findAll(), $conf);
    }

    function allPrepare()
    {
        $this->prepare();
    }

    function allAction()
    {
        $entity = $this->entity;
        $arrConstants = ['slug', 'title', 'name', 'email', 'name', 'key'];
        $arrHeading = ['id' => '#'];

        foreach ($arrConstants AS $constant) {
            if (defined(get_class($entity) . '::' . strtoupper($constant))) {
                $arrHeading[$constant] = $constant;
            }
        }

        return $this->_maestro->all($this->tableEntity->findAll(), [
            "title" => $this->_multiple,
            "heading" => $arrHeading,
        ]);
    }

    function addPrepare()
    {
        $this->prepare();
        $this->setFormByRecord();
        $this->record->checkPermissions('add');
    }

    function addAction($_data = [])
    {
        $this->fetchRequest();

        if (!isset($_data['title'])) $_data['title'] = __($this->_one);
        if (!isset($_data['content'])) $_data['content'] = $this->form->toHTML();

        // i18n configuration
        if (substr($this->table->getTable(), -5) == '_i18n' && isset($_data['submodule'])) {
            $_data['extends'] = "vendor/lfw/admin/src/Weblab/Admin/View/blank.twig";
            $_data['dontencapsulate'] = true;
        }

        return $this->_maestro->add($_data);
    }

    function editPrepare()
    {
        $this->prepare();
        $this->setFormByRecord();
        $this->record->checkPermissions('edit');
    }

    function editAction($_data = [])
    {
        $this->fetchRequest();

        if (!isset($_data['title'])) $_data['title'] = __($this->_one);
        if (!isset($_data['content'])) $_data['content'] = $this->form->toHTML();

        // controller after action
        if ($controller = $this->table->getController()) {
            $controller = context()->getController($controller);

            if (method_exists($controller, 'editAction')) {
                $_data['afterContent'] = Reflect::method($controller, 'editAction');
            }
        }
        if (!isset($_data['afterContent'])) {
            $_data['afterContent'] = null;
        }
        $_data['afterContent'] .= require_once path('root') . 'vendor/lfw/htmlbuilder/examples/index.php';

        // sidebar / related contents
        $_data['tabs'] = method_exists($this, "getRelatedAllAction")
            ? $this->getRelatedAllAction(request(), router(), $_data)
            : null;

        // i18n content
        if ($i18nRelation = ERelations::inst()->where(ERelations::TABLE1_ID, $this->table->getID())->where(ERelations::TABLE_RELATION_TYPE_ID, 5)->findOne()) {
            $_data['content'] .= $this->editI18nAction($i18nRelation);
        }

        // i18n configuration
        if (substr($this->table->getTable(), -5) == '_i18n' && isset($_data['submodule'])) {
            $_data['extends'] = "vendor/lfw/admin/src/Weblab/Admin/View/blank.twig";
            $_data['dontencapsulate'] = true;
        }

        return $this->_maestro->edit($_data);
    }

    function editI18nAction($i18nRelation)
    {
        if (substr($i18nRelation->getTable2()->getTable(), -5) != '_i18n') {
            die("not i18n relation");
        }

        // get languages
        $arrLanguages = \Base\Entity\Languages::inst()->where('enabled_frontend', true)->findAll();
        $arrTabs = [];

        foreach ($arrLanguages AS $language) {
            $langAdmin = new self;

            $recordI18nEntityString = $i18nRelation->getTable2()->getEnt();
            $recordI18nEntity = new $recordI18nEntityString;

            $recordI18n = $recordI18nEntity->where('language_id', $language->getId())
                ->where($i18nRelation->getTableField2()->getField(), $this->routerRecord)
                ->findOne();

            if (!$recordI18n) {
                //continue;
            }

            $langAdmin->setRouterId($i18nRelation->getTable2()->getId())
                ->setRouterRecord($recordI18n && $recordI18n->getId() ? $recordI18n->getId() : null)
                ->setTableEntity($this->getEntity('\Base\Entity\Tables'))
                ->setEntity($recordI18nEntity)
                ->setForm(form('\Weblab\Dynamic\Form\Records'))
                //  ->prepareFormI18n() // removes language && parent content
                ->prepare();

            if ($recordI18n) {
                $langAdmin->editPrepare();
                $language->content = $langAdmin->editAction(['submodule' => true]);
            } else {
                $langAdmin->addPrepare();
                $language->content = $langAdmin->addAction(['submodule' => true]);
            }
        }

        return view('Weblab/Maestro/View/editi18n', [
            'languages' => $arrLanguages,
        ]);
    }

    function getRelatedAllAction($arrControllers)
    { // TABLE_RELATION_TYPE_ID 5 = has translations, 3 = is parent
        $relations = ERelations::inst()
            ->where(ERelations::TABLE1_ID, $this->table->getID())
            ->where(ERelations::TABLE_RELATION_TYPE_ID, [1, 3, 5])->findAll();

        if (!$relations) return null;

        $arrData = null;

        foreach ($relations AS $relation) {
            $table2 = $relation->getTable2();
            $e = $table2->getEnt();
            $entity = new $e;
            $arrData[] = ["content" => self::allWhere($entity, [$relation->getTableField2()->getField() => $this->record->getId()], [
                "title" => $relation->getTitle(),
                "ctrl" => 'dynamic/tables/' . $table2->getId(),
            ]),
                "title" => $relation->getTitle()];
        }

        return $arrData;
    }

    function getRelatedEditAction()
    {

    }

    function deletePrepare()
    {
        $this->prepare();
        $this->record->checkPermissions('delete');
    }

    function deleteAction()
    {
        return $this->_maestro->delete($this->record, $this->_url, false);
    }

    function viewPrepare()
    {
        $this->prepare();
        $this->setFormByRecord();
        $this->record->checkPermissions('view');
    }

    function viewAction($_view)
    {
        return $this->_maestro->view([], [
            "title" => __($this->_one),
            "content" => '@T00D00',
        ]);
    }

    function mtmPrepare()
    {
        $this->prepare();
        $this->record->checkPermissions('edit');
        $this->form = $this->form->mtmAction($this->table, $this->record);
    }

    function mtmAction()
    {
        //$this->fetchRequest();

        if (!isset($_data['title'])) $_data['title'] = __($this->_one);
        if (!isset($_data['content'])) $_data['content'] = $this->form->toHTML();

        return $this->_maestro->mtm($_data);
    }
}