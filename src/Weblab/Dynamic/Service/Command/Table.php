<?php

namespace Weblab\Dynamic\Service\Command;


use Pckg\Database\Entity;
use Pckg\Database\Helper\Convention;
use Weblab\Dynamic\Entity\Fields;

class Table
{

    protected $entity = null;
    protected $table = null;
    protected $controller = null;

    protected $fields = [];
    protected $headings = [];
    protected $rowButtons = [];
    protected $records = [];
    protected $title = null;
    protected $description = null;
    protected $id = null;

    public function execute()
    {
        return view("vendor/lfw/admin/src/Weblab/Maestro/View/all", [
            "ext" => false,
            "data" => $this->records,
            "conf" => [
                "title" => $this->title,
                "description" => $this->description,
                "heading" => $this->headings,
                "url" => [
                    "delete" => "/dynamic/tables/" . $this->id . "/delete",
                    "edit" => "/dynamic/tables/" . $this->id . "/edit",
                    "view" => "/dynamic/tables/view",
                    "add" => "/dynamic/tables/" . $this->id . "/add",
                    "sort" => "/dynamic/tables/" . $this->id . "/sort",
                ],
                "rowbtn" => $this->rowButtons,
                "ctrl" => $this->controller,
                "table" => [
                    "class" => $this->table && $this->table->getTableDisplayType()->getSlug() == 'tree'
                        ? 'table-tree'
                        : 'datatable',
                ],
            ],
            "breadcrumbs" => [
                [
                    "title" => "Maestro",
                    "href" => "/maestro",
                ],
                [
                    "title" => Convention::toCamel($this->controller),
                ],
            ],
        ]);
    }

    public function setTable($table)
    {
        $this->table = $table;

        return $this;
    }

    public function setAllFromTable()
    {
        $this->table->checkPermissions();

        $this->setId($this->table->getId());
        $this->setTitle($this->table->getTitle());
        $this->setDescription($this->table->getDescription());
        $this->setController($this->table->getTitle());

        $this->setEntityFromTable();
        $this->setHeadingsFromTable();
        $this->setRowButtonsFromTable();
        $this->setRecordsFromTable();

        return $this;
    }

    public function setAllFromEntity()
    {
        $this->setHeadingsFromEntity();
        $this->setRecords($this->entity->findAll());

        return $this;
    }

    public function setEntity(Entity $entity)
    {
        $this->entity = $entity;

        return $this;
    }

    public function setHeadings($headings)
    {
        $this->headings = $headings;

        return $this;
    }

    public function setRecords($records)
    {
        $this->records = $records;

        return $this;
    }

    public function setRowButtons($rowButtons)
    {
        $this->rowButtons = $rowButtons;

        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function setController($controller)
    {
        $this->controller = $controller;

        return $this;
    }

    // entity
    public function setHeadingsFromEntity()
    {
        $arrConstants = array('slug', 'title', 'name', 'key');
        $arrHeading = array('id' => '#');

        foreach ($arrConstants AS $constant) {
            if (defined(get_class($this->entity) . '::' . strtoupper($constant))) {
                $arrHeading[$constant] = $constant;
            }
        }

        $this->setHeadings($arrHeading);

        return $this;
    }

    // table
    public function setEntityFromTable()
    {
        $entity = $this->table->getEnt();

        $this->setEntity(new $entity);

        return $this;
    }

    public function setHeadingsFromTable()
    {
        $arrFields = Fields::inst()->where(Fields::TABLE_ID, $this->table->getId())->orderBy(Fields::ORDER)->findAll();

        $arrHeading = [];
        foreach ($arrFields AS $field) {
            if ($field->isShown()) {
                $arrHeading[$field->getField()] = $field->getTitle();
            }
        }

        $this->setHeadings($arrHeading);

        return $this;
    }

    public function setRowButtonsFromTable()
    {
        $rowBtns = ["edit", "delete"];

        if ($this->table->getTable() == 'tables') {
            $rowBtns[] = "view";
        }

        $this->setRowButtons($rowBtns);

        return $this;
    }

    public function setRecordsFromTable()
    {
        $arrRecords = $this->entity->findAll();

        if ($this->table->getTableDisplayType()->getSlug() == 'tree') {
            $sortBy = $this->table->getTable() == 'settings'
                ? 'getSettingId'
                : ($this->table->getTable() == 'contents'
                    ? 'getParentContentId'
                    : ($this->table->getTable() == 'routes'
                        ? 'getParentRouteId'
                        : 'getMenuId'));
            $arrRecords = $arrRecords->sortBy($sortBy)->getTree($sortBy);
        }

        $this->records = $arrRecords;

        return $this;
    }

}